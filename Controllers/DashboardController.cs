﻿using System.Web.Mvc;

namespace TelegramBotsWebApplication.Areas.Customer.Controllers
{
    
    [Authorize(Roles = "Customer")]
    public class DashboardController:Controller
    {

        public ActionResult Index()
        {
            return View("Index");
        }
    }
}